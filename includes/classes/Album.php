<?php
  class Album {

    private $conn;
    private $id;
    private $title;
    private $artistId;
    private $genre;
    private $artworkPath;


    public function __construct($conn, $id) {
      $this->conn = $conn;
      $this->id = $id;

      $Query = mysqli_query($this->conn, "SELECT * FROM sjotify_albums WHERE id='$this->id'");
      $alblum = mysqli_fetch_array($Query);

      $this->title = $alblum['title'];
      $this->artistId = $alblum['artist'];
      $this->genre = $alblum['genre'];
      $this->artworkPath = $alblum['artworkPath'];

    }

    public function getTitle() {
      return $this->title;
    }

    public function getArtist() {
      return new Artist($this->conn, $this->artistId);
    }

    public function getGenre() {
      return $this->genre;
    }
      
    public function getArtworkPath() {
      return $this->artworkPath;
    }  

    public function getNumberOfSongs() {
      $Query = mysqli_query($this->conn, "SELECT id FROM sjotify_songs WHERE album='$this->id'");
      return mysqli_num_rows($Query);
    }

    public function getSongIds() {
      $Query = mysqli_query($this->conn, "SELECT id FROM sjotify_songs WHERE album='$this->id' ORDER BY albumOrder ASC");

      $array = array();
      
      while($row = mysqli_fetch_array($Query)) {
        array_push($array, $row['id']);
      }

      return $array;

    }

  }
?>
