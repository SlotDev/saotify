<?php
  ob_start();
  session_start();
  
  $timezone = date_default_timezone_set("Asia/Bangkok");

  $host = "mysql";
  // $host = "122.155.0.26";
  $userName = "admindb";
  $passName = "adminpass";
  $dbName = "snapshee_develop";

  $conn = mysqli_connect($host, $userName, $passName, $dbName);

  if(mysqli_connect_errno()) {
    echo "Failed to connect: " . mysqli_connect_errno();
  }

 ?>
